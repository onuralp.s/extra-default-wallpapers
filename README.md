# Extra Default Wallpapers

Project to create and maintain a set of extra default wallpapers that Fedora users can choose as an alternative to the default release wallpaper.

## Goal

We will produce at least 6 abstract wallpapers (with both light and dark versions for each) as a set for Fedora 37.

## Todo/Timeline

[x] Research what (if any) types of narrative themes tie together default wallpaper sets on various platforms (OS X, iOS, Android, Samsung, etc.)
[x] Determine theme to use (if any) to connect designs
[x] Brainstorm concepts
[x] Sketch concepts
[x] Start implementing in Blender
[ ] Map concepts to mockups
[ ] Finalize light & dark versions of mockups
[ ] Final selection and preparation of wallpapers

## Communications:

[x] [Initial draft blog post](https://blog.linuxgrrl.com/2022/06/27/abstract-wallpapers-in-blender-using-geometry-nodes/) - [shared to discussions.fpo](https://discussion.fedoraproject.org/t/fedora-extra-wallpapers-project/39962) and [on Twitter](https://twitter.com/mairin/status/1541460444447215616) as well

## Specifications

* 4096x4096 - to either be cropped manually or cropped in software. This means design must work both cropped horizontally and vertically.
* Supports the day/night feature - a quick cross fade transition between the two - so each wallpaper will have a light version and a dark version
* Abstract - because of the aspect ratio and light/dark mode requirement, we'll start by making an abstract set
* webp format - it is smaller and GNOME is looking to move this way.

## Resources

* [GNOME HIG Background Guidelines](https://developer.gnome.org/hig/reference/backgrounds.html)
*jimmac's livestreams of creating the GNOME ones in Blender: https://www.twitch.tv/videos/1480042075 & https://www.twitch.tv/videos/1479336862
